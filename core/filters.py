import django_filters as filters
from core import models


class ContactFilter(filters.FilterSet):
    name = filters.NumberFilter(label="id", field_name='id', lookup_expr="exact")
    count = filters.CharFilter(label="Организация", field_name='organization', lookup_expr="icontains")

    class Meta:
        model = models.Contact
        fields = ['id', 'organization']
