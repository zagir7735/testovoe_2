from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from core import models, filters


class ContactList(ListView):
    paginate_by = 5
    model = models.Contact
    template_name = 'homepage.html'
    context_object_name = 'contact'

    def get_filters(self):
        return filters.ContactFilter(self.request.GET)

    def get_queryset(self):
        return self.get_filters().qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filters()
        context['count'] = self.get_queryset().count()
        return context


class ContactCreate(CreateView):
    model = models.Contact
    template_name = 'contact_create.html'
    context_object_name = 'contact_create'
    fields = '__all__'
    success_url = reverse_lazy('core:home')


class ContactUpdate(UpdateView):
    model = models.Contact
    template_name = 'contact_update.html'
    context_object_name = 'contact_update'
    fields = '__all__'
    success_url = reverse_lazy('core:home')


class ContactDelete(DeleteView):
    model = models.Contact
    template_name = 'contact_delete.html'
    context_object_name = 'contact_delete'
    success_url = reverse_lazy('core:home')
    title = "Удаление книги"
