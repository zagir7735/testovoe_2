from django.urls import path

from core.views import ContactList, ContactCreate, ContactUpdate, ContactDelete

app_name = 'core'

urlpatterns = [
    path('', ContactList.as_view(), name='home'),
    path('contact_create/', ContactCreate.as_view(), name='contact_create'),
    path('contact_update/<int:pk>', ContactUpdate.as_view(), name='contact_update'),
    path('contact_delete/<int:pk>', ContactDelete.as_view(), name='contact_delete')
]
