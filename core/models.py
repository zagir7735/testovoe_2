from django.db import models
from phone_field import PhoneField


class Contact(models.Model):
    last_name = models.CharField('Фамилия контакта', max_length=255, blank=True)
    first_name = models.CharField('Имя контакта', max_length=255, blank=True)
    surname = models.CharField('Отчество контакта', max_length=255, blank=True)
    organization = models.CharField('Организация', max_length=255, blank=True)
    job_telephone = PhoneField('Рабочий номер телефона', blank=True, unique=True)
    personal_telephone = PhoneField('Сотовый номер телефона', blank=True, unique=True)

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return f'{self.last_name}, {self.first_name}', {self.surname}
