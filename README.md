# testovoe_2


Было изучено [техническое задание](https://docs.google.com/document/d/1dIH7lY05hNLSluZgOYsRyTrvLmyz4CnNEtJFFXBbS-c/edit).
Реализация включала в себя создание модели "Contact" и написание представлений.

**Были написаны вьюшки (представления):**
- Класс `ContactList` выдает список всех контактов. Также выводится кол-во контактов в справочнике и реализована фильтрация по полям "id" и "организация".
- Класс `ContactCreate` добавляет контакт к справочнику.
- Класс `ContactUpdate` изменяет существующий в справочнике контакт.
- Класс `ContactDelete` удаляет существующий в справочнике контакт.

**Были написаны темплейты: "домашняя страница", "создание контакта", "изменение контакта", "удаление контакта". **

# Источники:
- `django` - [Документация по django](https://docs.djangoproject.com/en/5.0/)
- `django_phonenumber` - [Документация по библиотеке django_phonenumber](https://django-phonenumber-field.readthedocs.io/en/latest/) - использовался для полей номеров телефонов.
- Неоднократно пользовался ресурсом [Stack Overflow](https://stackoverflow.com/) и пару видео с ресурса [YouTube](https://youtube.com/)

Этот проект был создан в рамках работы над тестовым заданием. Допущены несоответствия тех. заданию. Основная цель проекта - показать навыки 
по работе с фреймворком Django.
